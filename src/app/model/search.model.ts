export interface Search {
    region?:            string;
    checkInDate?:       Date;
    checkOutDate?:      Date;
    adults?:            number;
    rooms?:             number;
    children?:          number;
}