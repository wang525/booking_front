import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { HttpParams } from  "@angular/common/http";
import { environment } from "../../../environments/environment";



let header = new HttpHeaders({
    'X-Api-Key': '5042ae43-516c-4649-9f1a-79e5462628c5',
    'X-Session': 'bf1882b665bf480caec3f99ad32b77a7'
});

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private http: HttpClient,
    ) { }

    getApiUrl(subPath) {
        return environment.baseApiUrl + subPath;
    }

    getHotelLists(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/hotel-list"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }

    getHotelDetail(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/hotel-detail"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }

    getRegionByTerm(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/autosuggest"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }

    bookPolicy(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/bookpolicy"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }

    preBook(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/prebook"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }

    cancelBook(data) {
        return new Promise((resolve, reject) => {
            this.http
            .post(this.getApiUrl("/cancelbook"), data)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    resolve('err');
                }
            );
        });
    }
}
