import { LayoutComponent } from '../layout/layout.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'hotel', loadChildren: './hotel/hotel.module#HotelModule' },
        ]
    },
    

    // Not found
    { path: '**', redirectTo: 'home' }

];
