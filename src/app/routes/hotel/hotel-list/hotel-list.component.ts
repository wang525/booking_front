import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Search } from '../../../model/search.model'
import { ApiService } from '../../../core/service/api.service';

@Component({
    selector: 'app-hotel-list',
    templateUrl: './hotel-list.component.html',
    styleUrls: ['./hotel-list.component.scss']
})
export class HotelListComponent implements OnInit {

    private sub: any;
    search: Search;

    hotelList: Array<any> = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public api: ApiService,
    ) { }

    ngOnInit() {
        this.sub = this.route.queryParams.subscribe(params => {
            this.search = params;
            
            if (this.search.region && this.search.checkInDate && this.search.checkOutDate && this.search.adults && this.search.rooms) {
                this.api.getHotelLists(this.search).then(res => {
                    this.hotelList = [];
                    this.hotelList = Object.keys(res).map((k) => res[k]);
                })
            }

        });
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    gotoDetail(hotel) {
        if (this.search) {
            let data = {
                hotel_id: hotel.id,
                check_in_date: this.search.checkInDate,
                check_out_date: this.search.checkOutDate,
                adult_count: this.search.adults,
                room_count: this.search.rooms,
                children: this.search.children
            }
            
            this.router.navigate(['hotel/detail'], {queryParams: data});
        }
        
    }

}
