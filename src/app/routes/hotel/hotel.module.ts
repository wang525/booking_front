import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelDetailComponent } from './hotel-detail/hotel-detail.component';


const routes: Routes = [

    { path: 'list', component: HotelListComponent },
    { path: 'detail', component: HotelDetailComponent },


];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        HotelListComponent, 
        HotelDetailComponent
    ]
})
export class HotelModule { }
