import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Search } from '../../../model/search.model'
import { ApiService } from '../../../core/service/api.service';

declare var $;

@Component({
    selector: 'app-hotel-detail',
    templateUrl: './hotel-detail.component.html',
    styleUrls: ['./hotel-detail.component.scss']
})
export class HotelDetailComponent implements OnInit {

    private sub: any;
    hotelDetail: any;
    roomDetail: any;
    hotelId: string;
    search: any;

    bookinPolicy: any;
    booking: any;

    bookingId: any;

    isBookable: boolean = false;
    personInfo: any;

    bookingStatus: any = null;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public api: ApiService,
    ) { }

    ngOnInit() {
        this.personInfo = {
            "salutation": "Mr.",
            "first_name": "Jack",
            "last_name": "Wang",
            "email": "jack.wang@zumata.com",
            "city": "ShenYang",
            "state": "CN",
            "street": "Pujiang",
            "postal_code": "90265",
            "country": "CN",
            "nationality": "CN",
            "contact_no": "16781244"
        }
        this.hotelDetail = {};
        this.bookingStatus = null;

        this.sub = this.route.queryParams.subscribe(params => {
            this.hotelDetail = params;
            
            if (this.hotelDetail.hotel_id && this.hotelDetail.check_in_date && this.hotelDetail.check_out_date && this.hotelDetail.adult_count && this.hotelDetail.room_count) {
                this.api.getHotelDetail(this.hotelDetail).then(res => {
                    // console.log('hotel detail res==', res)
                    this.hotelDetail = res['hotel_detail'];

                    if (this.hotelDetail && this.hotelDetail.image_details && this.hotelDetail.image_details.count > 0) {
                        let tmp = [];
                        for (var i = 0; i < this.hotelDetail.image_details.count; i++) {
                            tmp.push(this.hotelDetail.image_details.prefix+i+this.hotelDetail.image_details.suffix)
                        }
                        this.hotelDetail['images'] = tmp;
                    }

                    if (res['room_detail'] && res['room_detail']['hotels'] && res['room_detail']['hotels'][0] &&
                        res['room_detail']['hotels'][0]['rates'] && res['room_detail']['hotels'][0]['rates']['packages']) {
                        this.roomDetail = res['room_detail']['hotels'][0]['rates']['packages'];
                    }

                    if (res['room_detail'] && res['room_detail']['search']) {
                        this.search = res['room_detail']['search'];
                    }
                })
            }

        });
    }

    book(roomPackage) {

        let data = {
            search: this.search,
            package: roomPackage
        }
        this.api.bookPolicy(data).then(res => {
            this.bookingStatus = null;
            this.isBookable = true;
            this.bookinPolicy = res;
        })
    }

    preBook() {
        let roomLeadGuest = {
            "first_name": "Charlie",
            "last_name": "Smith",
            "nationality": "US"
        }

        let roomGuests = [];
        // only for test:
        if (this.search.room_count > 0) {
            for (var i = 0; i < this.search.room_count; i++) {
                roomGuests.push(roomLeadGuest);
            }
        }

        let data = {
            booking_policy_id: this.bookinPolicy.booking_policy_id,
            client_reference: '7682197',
            room_lead_guests: roomGuests,
            contact_person: this.personInfo
        }

        this.api.preBook(data).then(res => {
            this.booking = res;
            this.bookingId = res['booking_id'];

            if (this.booking && this.booking.status) {
                this.bookingStatus = this.booking.status;
                let alarm = this.alarmInfo(this.bookingStatus);
                
                alert(alarm);
                // this.isBookable = false;
            }
        })
    }

    cancelBook() {
        if (this.bookingStatus == 'bkg-cf' && this.bookingId) {
            let data = {
                booking_id: this.bookingId
            }
            this.api.cancelBook(data).then(res => {
                this.bookingStatus = res['status'];
                let alarm = this.alarmInfo(this.bookingStatus);
                alert(alarm)
            })
        }
    }

    alarmInfo(status) {
        let alarm;
        if (status === 'bkg-ns') {
            alarm = 'Initial state'
        }
        else if (status === 'bkg-ip') {
            alarm = 'Booking in progress.'
        }
        else if (status === 'bkg-cf') {
            alarm = 'Confirmed.'
        }
        else if (status === 'bkg-cx') {
            alarm = 'Cancelled.'
        }
        else if (status === 'bkg-af') {
            alarm = 'Booking acquisition failed.'
        }
        else if (status === 'bkg-ps') {
            alarm = 'Confirmation pending on the supplier side.'
        }
        return alarm;
    }
}
