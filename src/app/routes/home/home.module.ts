import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    { path: '', component: HomeComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgSelectModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule, 
        OwlNativeDateTimeModule,
        CommonModule,
    ],
    declarations: [
        HomeComponent
    ],
    exports: [
        RouterModule,
        OwlDateTimeModule, 
        OwlNativeDateTimeModule,
    ]
})
export class HomeModule { }
