import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject} from 'rxjs';
import { distinctUntilChanged, debounceTime, take } from 'rxjs/operators'

import { Search } from '../../../model/search.model'
import { ApiService } from '../../../core/service/api.service';

const dateFormat = require('dateformat');

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    search: Search;
    itemsCities: Array<any>;
    searchForm: FormGroup;
    editingStatus:boolean = false;
    showRooms: any;

    regionSearchTerm$ = new Subject<string>();
    subRegion: any = null;

    constructor(
        private router: Router,
        public api: ApiService,
        fb: FormBuilder
    ) { 
        this.search ={
            region: ''
        }
        this.searchForm = fb.group({
            'region': [null],
            'checkInDate': [null],
            'checkOutDate': [null],
        });
    }

    ngOnInit() {
        this.search.adults = 2;
        this.search.rooms = 1;
        this.search.children = 0;
        
        // static data for city
        
        this.setRooms();

        this.subRegion = this.regionSearchTerm$.pipe(
            debounceTime(200),
            distinctUntilChanged()).
            subscribe(term => {

                this.api.getRegionByTerm({term:term}).then(res => {
                    if (res['results'] && res['results']['city_en_us']) {
                        this.itemsCities = [];
                        this.itemsCities = res['results']['city_en_us'];
                    }
                })
            })
    }

    ngOnDestroy() {
        if (this.subRegion) {
            this.subRegion.unsubscribe();
        }
    }
    getFocused() {
        this.editingStatus = true;
    }

    modelChanged($val, field) {
        if ($val != undefined) {
            this.search[field] = $val;
            if (field == 'checkInDate' || field == 'checkOutDate') {
                this.search[field] = dateFormat($val, 'yyyy-mm-dd')
            }
        }
    }

    changeRange() {
        
    }

    increaseCnt(field) {
        if (this.search[field] >= 0) {
            this.search[field] = this.search[field] + 1;
        }
    }
    
    decreaseCnt(field) {
        if (this.search[field] >= 1) {
            this.search[field] = this.search[field] - 1;
        }
    }

    setRooms() {
        this.showRooms = this.search.adults + ' adults, ' + this.search.rooms + ' rooms, ' + this.search.children + ' childrens';
        this.editingStatus = false;
    }

    

    searchHotels() {
        if (this.search.region && this.search.checkInDate && this.search.checkOutDate) {
            this.router.navigate(['hotel/list'], { queryParams: { 
                region:this.search.region,
                checkInDate: this.search.checkInDate,
                checkOutDate: this.search.checkOutDate,
                adults: this.search.adults,
                rooms: this.search.rooms,
                children: this.search.children
            } });
        }
        else {
            alert('Please fill the content');
        }
    }
}
